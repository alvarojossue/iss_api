## Is the ISS near me?

An API that returns the distance between you and the International Space Station (ISS)'s current location.

## Technologies

This API is enterily written in JavaScript, using Node.js as the server-side enviroment and Hapi.js as its framework.

## Usage

* Install the dependencies:

`npm install`

* Run the dev server at localhost:4000: 

`node index.js`

* Call endpoints:

`/api/distance`

To retrieve the distance between the ISS and you

`/api/location`

To retrieve the current location of the ISS

